import React from 'react';
import Form from './components/FormPage/Form'
import Header from './components/Header/Header';


class App extends React.Component {
  render() {
    return (
      <div className="App container">
        <Header/>
        <Form/>
      </div>
    );
  }
  
}

export default App;
