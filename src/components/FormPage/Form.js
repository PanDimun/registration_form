import React from "react";
import './Form.css'
import { FormErrors } from "./FormErrors";

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      formErrors: {email: '', password: ''},
      emailValid: false,
      passwordValid: false,
      formValid: false,
      showPassword: false,
    };
    this.handleUserInput = this.handleUserInput.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.validateField = this.validateField.bind(this);
    this.validateForm = this.validateForm.bind(this);
    this.errorClass = this.errorClass.bind(this);
    this.togglePassword = this.togglePassword.bind(this)
  };
  handleUserInput(e) {
    const name = e.target.name;
    const value = e.target.value;
    this.setState({ [name] : value }, () => {this.validateField(name, value)});
  };

  handleSubmit(event) {
    event.preventDefault();
    alert('Пользователь ' + this.state.email + ' зарегистрирован!');
    this.setState({ email: '', password: '' })
  };

  validateField(fieldName, value) {
    let fieldValidationErrors = this.state.formErrors;
    let emailValid = this.state.emailValid;
    let passwordValid = this.state.passwordValid;
    switch(fieldName) {
        case 'email':
          emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
          fieldValidationErrors.email = emailValid ? '' : ' не корректный';
          break;
        case 'password':
          passwordValid = value.length >= 6;
          fieldValidationErrors.password = passwordValid ? '': ' слишком короткий';
          break;
        default:
          break;
    }
    this.setState({ formErrors: fieldValidationErrors,
                    emailValid: emailValid,
                    passwordValid: passwordValid
                  }, this.validateForm);
  };

  validateForm() {
    this.setState({formValid: this.state.emailValid && this.state.passwordValid});
  };

  errorClass(error) {
    return(error.length === 0 ? '' : 'has-error');
  };
  togglePassword() {
    this.setState( {showPassword: !this.state.showPassword})
  }

  render() {
    return (
      <div className="form">
        <div className="form__header">
          <h3>Зарегистрироваться</h3>
        </div>
        <div className="panel panel-default"></div>
        <form onSubmit={this.handleSubmit}>
          <div className={`mb-3 ${this.errorClass(this.state.formErrors.email)}`}>
            <label htmlFor="exampleInputEmail1" 
            className="form-label">
              Email address
            </label>
            <input name="email" 
              type="email" 
              className="form-control" 
              id="exampleInputEmail1" 
              aria-describedby="emailHelp" 
              value={this.state.email} 
              onChange={this.handleUserInput} />
          </div>
          <div className="panel panel-default"></div>
          <div className={`mb-3 ${this.errorClass(this.state.formErrors.password)} _password-icon-position`}>
            <label 
              htmlFor="exampleInputPassword1" 
              className="form-label">
                Password
            </label>
            <div class="input-group mb-3">
              <input name="password" 
                type={this.state.showPassword ? "text" : "password"} 
                className="form-control" 
                id="exampleInputPassword1" 
                value={this.state.password} 
                onChange={this.handleUserInput} 
                aria-describedby="button-addon2"/>
              <button className="btn btn-outline-secondary" 
                type="button" 
                id="button-addon2">
                  <i onClick={this.togglePassword} 
                  class={`fa ${this.state.showPassword ? "fa-eye-slash" : "fa-eye"}`} 
                  id="togglePassword">
                  </i>
              </button>
            </div>
          </div>
          <div className="panel panel-default"></div>
          <button type="submit" 
            className="btn btn-primary" 
            disabled={!this.state.formValid}>
              Submit
            </button>
          <div className="form__alertErrors">
            { this.state.formErrors.email || this.state.formErrors.password ? <FormErrors formErrors={this.state.formErrors} /> : "" }  
          </div>
        </form>
      </div>
      )
  };
};

export default Form;